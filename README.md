# INDYSCCNAMD

NAMD helper materials for the student cluster competition. This includes detailed instructions for downloading and installing NAMD on the cluster, and what is needed to do the homework/tutorial for running NAMD.

## Read through the [seminar slides](NAMDIndySC24.pptx)

## Do the [Homework](Homework.md)
