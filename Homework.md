# Installing a precompiled NAMD build

The most straightforward way to get started with NAMD is to use a precompiled NAMD build from the [NAMD download page](https://www.ks.uiuc.edu/Development/Download/download.cgi?PackageName=NAMD).
We will be assuming a Linux multicore build, which cannot communicate across a network, to get a feel for how NAMD works, and what normal output looks like.
Clicking on the [Linux-x86_64-multicore](https://www.ks.uiuc.edu/Development/Download/download.cgi?UserID=&AccessCode=&ArchiveID=1714) link under version 3.0 on the page will trigger a download after user registration.
Registration is required as part of the NAMD license, and is triggered by choosing a username and password.
This is needed so that NAMD developers can collect user statistics that are passed to the funding agency that supports NAMD, the National Institutes of Health (NIH).
Move the downloaded file to the cluster, and untar the install as follows:

```bash
tar -zxf NAMD_3.0_Linux-x86_64-multicore.tar.gz
```

This creates a `NAMD_3.0_Linux-x86_64-multicore` directory, and inside the directory, there is a `namd3` binary that we'd like to run.

```bash
cd NAMD_3.0_Linux-x86_64-multicore
./namd3 +p2
```

This will complain that `FATAL ERROR: No simulation config file specified on command line.`, highlighting that we need to tell NAMD what we actually want to simulate.
Fair enough, but take a look at the rest of the text that is spat out, and answer the following questions:
- In the Charm++ lines, how many processors are detected on the node?
- How many processors was NAMD running on?
- Which command line argument controlled that behavior?

# Benchmarking (single node) NAMD on CPUs

The key metric that scientists care about is how much simulated time can be collected within a day of walltime, and is usually reported in nanoseconds (of simulated time) per day (of walltime), abbreviated ns/day.
Ideally, the number of ns/day will grow linearly with the amount of computational horsepower allocated to the task, since Charm++ will split the computational task among all the processors.
There are a number of benchmark systems available from the [NAMD developers](https://www.ks.uiuc.edu/Research/namd/utilities/), but for now, we'll focus on ApoA1.![ApoA1](./apoa1.png)
The ApoA1 protein (blue and red cartoon) wraps around lipids, creating a little lipid raft (represented by spheres, gray for carbons, red for oxygens, blue for nitrogens, tan for phosphorus, hydrogens are hidden).
Once solvated in water (blue surface), the total system is about 92,000 atoms large, representative of many small protein systems.
What we'll be doing is using this small system as a benchmark, which we will first download and untar.
```bash
wget https://www.ks.uiuc.edu/Research/namd/utilities/apoa1.tar.gz
tar -zxf apoa1.tar.gz
cd apoa1
```

All of the files contained within are human-readable.
`apoa1.namd` in particular is interesting, since that is the NAMD-configuration file that sets how the simulation will proceed.
You can lookup what all of the parameters mean within the [NAMD user guide](https://www.ks.uiuc.edu/Research/namd/3.0/ug/), but for our initial forrays, the default of 500 steps with 1 femtosecond elapsing between steps is fine.
This is what the bare-bones slurm submission script (`submit.sh`) would look like:
```bash
#!/bin/bash
#SBATCH -n 1

$HOME/NAMD_3.0_Linux-x86_64-multicore/namd3 +p$SLURM_CPUS_PER_TASK apoa1.namd > apoa1-$SLURM_CPUS_PER_TASK.log
```
Then the simulations could be submitted with something like `sbatch -c 4 submit.sh`.
Run this simulation for 1, 2, 4, 8, and 16 processors, generating multiple files like `apoa1-*.log`.
The logs contain a wealth of information, including startup information and the energy at every time.
For performance measurement, the most interesting lines are the benchmark lines, which can be picked out with `grep Benchmark *log`.
The `ns_per_day.py` script provided in the gitlab repository can also be used to determine performance.
Using your favorite plotting software, plot the performance (y-axis) against the number of cores (x-axis).
Label your axes!
The trend should be approximately linear.
- How many ns/day would you achieve with 1 core?
- What performance could you expect from all 16 cores on the node?

Another relevant quantity is the efficiency, which expresses how well the work parallelizes across multiple cores.
The formula is (performance/cores)/(reference performance/reference cores), where the reference is chosen as the state with the best performance/core ratio (usually the least parallel version).
This gives you an efficiency between 0 and 1.
Plot the efficiency using your favorite plotting software, with the efficiency on the y-axis vs the number of cores on the x-axis, and labeling your axes.
- Describe the trend for the efficiency

# Benchmarking (single node) NAMD on GPUs

NAMD builds support GPUs only with compile-time switches, and so a pre-built NAMD that uses GPUs is a [different download](https://www.ks.uiuc.edu/Development/Download/download.cgi?UserID=&AccessCode=&ArchiveID=1713).
Modifying `submit.sh` to call the GPU binary, we can rerun the benchmarks to observe the acceleration.
By default, NAMD will use the GPU-offload compute model that was the default in NAMD 2.14.
To activate the GPU-resident model, the NAMD configuration file would need to be rerun with `GPUresident on`.
- Compare CPU, GPU-offload and GPU-resident performance

# Building multinode NAMD **Extra**

The precompiled NAMD versions either are restricted to a single node, or can't maximally benefit from the interconnect in place.
So we'd like to compile our own NAMD build, with Charm++ resting on top of MPI, as an example of what is possible as you try to extract maximum performance from NAMD.
NAMD is easiest to compile with OpenMPI coupled to the GNU compiler toolchain.
```bash
wget https://download.open-mpi.org/release/open-mpi/v5.0/openmpi-5.0.5.tar.gz
tar -zxf openmpi-5.0.5.tar.gz 
cd openmpi-5.0.5
./configure --prefix="$HOME/openmpi-5.0.5-local"
make -j8
make install
export PATH=$HOME/openmpi-5.0.5-local/bin:$PATH
```

First, we will need to get the NAMD source from the [NAMD download page](https://www.ks.uiuc.edu/Development/Download/download.cgi?PackageName=NAMD) as before, this time selecting the [3.0 source](https://www.ks.uiuc.edu/Development/Download/download.cgi?UserID=&AccessCode=&ArchiveID=1712) package.
This is then untarred as usual.

```bash
tar -zxf NAMD_3.0_Source.tar.gz
cd NAMD_3.0_Source
tar -xf charm-8.0.0.tar
cd charm-8.0.0
```

Now we'd need to build Charm++.
I highly recommend running the `./buildold` script at least once, which launches a smart configurator that asks about what sort of Charm++ build you'd like to try, including if you want to use OFI or Infiniband Verbs, or if you'd like to try SMP options for Charm++.
At the end of the configurator, just before building, you will see a line like: `./build charm++ mpi-linux-x86_64   mpicxx -j16  --with-production` that you can use to skip going through the configurator and to start building immeadiately.
The new (and in my opinion not improved way) of doing this is to look at the [Charm++ documentation](https://charm.rtfd.io/en/latest/charm++/manual.html#installing-charm), which will help you figure out the appropriate options for your interconnect.

```bash
./build charm++ mpi-linux-x86_64  mpicxx -j16  --with-production
```

This should build Charm++, and get you ready to build NAMD proper, once we have the other preliminaries dealt with.
NAMD's configuration file is really a tcl script, and so we need a tcl library in place.
We additionally need a fast fourier transform library to do long-range electrostatics.
We can get versions that work from the NAMD developers, but do keep in mind that licensing means that the versions distributed this way are ancient, and may be a way to boost the performance by modernizing those libraries.

```bash
#Go back to main NAMD_3.0_Source directory
cd ..
#Get fftw
wget http://www.ks.uiuc.edu/Research/namd/libraries/fftw-linux-x86_64.tar.gz
tar xzf fftw-linux-x86_64.tar.gz
mv linux-x86_64 fftw
#Get tcl
wget https://www.ks.uiuc.edu/Research/namd/libraries/tcl8.6.13-linux-x86_64-threaded.tar.gz
tar xzf tcl8.6.13-linux-x86_64-threaded.tar.gz
mv tcl8.6.13-linux-x86_64-threaded tcl-threaded
#Configure NAMD
./config Linux-x86_64-g++ --charm-arch mpi-linux-x86_64-mpicxx --with-fftw --tcl-prefix `pwd`/tcl-threaded
cd Linux-x86_64-g++/
#Build NAMD
make -j8
```

At the end of this configuration and compilation process, you should have a `namd3` binary built.
The best hints for how to control the NAMD compilation process are in the `config` file, which is really a bash script rather than a part of the autoconf toolchain.
Together with locations specified in the arch directory, which controls where the build process looks for external libraries like FFTW, these are some of the biggest decisions you'll get to make when building NAMD.

# Benchmarking (multi-node) NAMD **Extra**

In an ideal world, we would have a way for multiple nodes to run a single job.
Normally, this is done by taking this MPI executable, and tying it into the submission system.
The ApoA1 simulation system is again going to be our test-bed for benchmarking. A simple script like the following could be submitted to your queuing system.

```bash
#!/bin/bash
mpirun -np $SLURM_NTASKS $HOME/NAMD_3.0_Source/Linux-x86_64-g++/namd3 apoa1.namd > mpiapoa1-$SLURM_NTASKS.log
```
Then, submitting jobs with something like `sbatch -n 300 submit-mpi.sh` will divide the simulation over the total number of MPI tasks given.
If getting slurm running is too messy, you can also run mpi executables with a hostname file.

The key questions now are basically the same as what we had before in the single-node case.
- How fast can you simulate the ApoA1 system with the hardware at your disposal?
- How efficiently can NAMD scale across multiple nodes?
- Are there potentially NAMD configuration options you could change to improve performance?

# Next steps and other resources

An SMP build of NAMD is roughly analogous to how OpenMP and MPI can work together to maximally exploit the parallelism present in the hardware.
The [NAMD User Guide](https://www.ks.uiuc.edu/Research/namd/3.0/ug/node92.html) has resources to help you craft your run scripts and compilation approaches.
For multiple (mostly GPU-based) high performance computing systems, [abbreviated](https://github.com/jvermaas/Software-Building-Instructions/blob/main/NAMD.md) and [somewhat longer](https://docs.hpc.wvu.edu/text/609.CHARM++_NAMD.html#) compilation instructions exist, beyond what is provided in the `notes.txt` file within the NAMD source tarball.
